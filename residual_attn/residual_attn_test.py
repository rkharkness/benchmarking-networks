import pandas as pd
from collections import Counter
import albumentations as A

from keras.preprocessing.image import ImageDataGenerator
from keras.applications import VGG16, VGG19, DenseNet121, DenseNet201, mobilenet, ResNet50
from keras.layers import AveragePooling2D
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers import Dense
from keras.layers import Input
from keras.models import Model
from keras.optimizers import Adam
from keras.utils import to_categorical
from keras.callbacks import ModelCheckpoint

from keras.layers import BatchNormalization
from keras.layers import Conv2D
from keras.layers import UpSampling2D
from keras.layers import Activation
from keras.layers import MaxPool2D
from keras.layers import Add
from keras.layers import Multiply
from keras.layers import Lambda

from keras.regularizers import l2

from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
import numpy as np
import argparse
import cv2
import os
from keras import backend as K
import random

import keras as keras
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sb

from keras.models import load_model

from residual_attn_train import residual_block, attention_block, AttentionResNet56
from utils.tf_callbacks import recall_m, precision_m, f1_m, PlotLosses

import tensorflow as tf

parser = argparse.ArgumentParser(description='Residual Attention Net Testing Script')

parser.add_argument('--bs', default=16, type=int, help='Batch size')
parser.add_argument('--img_size', default=320, type=int, help='Image size')
parser.add_argument('--img_channel', default=1, type=int, help='Image channel')
parser.add_argument('--data_csv', default='/MULTIX/DATA/HOME/custom_data_ablation_1500.csv', type=str, help='Path to data file')
parser.add_argument('--save_dir', default='/MULTIX/DATA/HOME/residual_attn', type=str, help='Name of folder to store training checkpoints')
parser.add_argument('--data_dir', default='/home/ubuntu/custom_data/', type=str, help='Path to data folder')
parser.add_argument('--weights_path', default='residual_attn', help='Core filename for saved weights (excl. _fold and .h5')

args = parser.parse_args()

def convert_path(df):
  """convert img paths for compatibility with MULTIX env"""
  paths = df['savepath'].str.split('/')
  
  new_paths = []
  for i,v in enumerate(paths):
    v = v[1:]
    v[0] ="/MULTIX/DATA/HOME"
    v = "/".join(v)
    new_paths.append(v)
  df['multix_savepath'] = new_paths

  return df

total_data = pd.read_csv(args.data_csv, dtype=str)
print(total_data.head())

if 'split' in total_data.columns:
    test_df = total_data[total_data['split']=='test']
    test_df = test_df.reset_index(drop=True)

elif 'savepath' in total_data.columns:
    total_data = convert_path(total_data)
    test_df = total_data[total_data['multix_savepath'] =='/MULTIX/DATA/HOME/covidx_data/train']
    test_df['pneumonia_binary'] = test_df['binary'].values
    test_df = test_df.reset_index(drop=True)
    test_df["filename"] = test_df["multix_savepath"] + os.sep + test_df['finding'] + os.sep + test_df["filename"]
    test_df = test_df.reset_index(drop=True)

print(test_df.head())
print(test_df.filename)

def test_transform(image):
    image = cv2.resize(image, (args.img_size, args.img_size), interpolation = cv2.INTER_AREA)
    image = np.expand_dims(image, -1)
    image = image.astype(np.float64)
    return image

test_datagen=ImageDataGenerator(
            preprocessing_function=test_transform,
            rescale=1/255.0)

test_generator=test_datagen.flow_from_dataframe(dataframe=test_df, directory=args.data_dir,
                                                    x_col="filename", y_col='pneumonia_binary', class_mode="categorical", target_size=(args.img_size,args.img_size), color_mode='grayscale',
                                                    batch_size=args.bs, shuffle=False)

STEP_SIZE_TEST=test_generator.n//test_generator.batch_size

model = AttentionResNet56(shape=(args.img_size, args.img_size, args.img_channel),n_channels=args.img_channel, n_classes=2,
                      dropout=0.2, regularization=1e-3)

model.compile(keras.optimizers.Adam(lr=1e-3), loss='categorical_crossentropy', metrics=['accuracy', f1_m, precision_m, recall_m])

def get_model_name(k):
    return args.weights_path + '_' + str(k) + '.h5'

cv_accuracy = []
cv_loss = []

fold_no = [1,2,3]
for i in fold_no:
    filepath = args.save_dir+get_model_name(i)
    model.load_weights(filepath)
    with tf.device('/device:GPU:0'):
        scores = model.evaluate(test_generator, steps=STEP_SIZE_TEST)

        y_pred = model.predict(test_generator)
        y_pred = np.argmax(y_pred, axis=1)
        y_test = test_generator.classes
        matrix = confusion_matrix(y_test, y_pred)
        print(matrix)

        print(f'Score for fold {fold_no}: {model.metrics_names[0]} of {scores[0]}; {model.metrics_names[1]} of {scores[1]*100}%')

        cv_accuracy.append(scores[1]*100)
        cv_loss.append(scores[0])

# == Provide average scores ==
print('------------------------------------------------------------------------')
print('Score per fold')
for i in range(0, len(cv_accuracy)):
  print('------------------------------------------------------------------------')
  print(f'> Fold {i+1} - Loss: {cv_loss[i]} - Accuracy: {cv_accuracy[i]}%')
print('------------------------------------------------------------------------')
print('Average scores for all folds:')
print(f'> Accuracy: {np.mean(cv_accuracy)} (+- {np.std(cv_accuracy)})')
print(f'> Loss: {np.mean(cv_loss)}')
print('------------------------------------------------------------------------')

