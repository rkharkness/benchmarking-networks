# from __future__ import print_function

import albumentations as A

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import argparse
import cv2
import random

from keras.preprocessing.image import ImageDataGenerator
from keras.layers import AveragePooling2D
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers import Dense
from keras.layers import Input
from keras.models import Model

from keras.layers import BatchNormalization
from keras.layers import Conv2D
from keras.layers import UpSampling2D
from keras.layers import Activation
from keras.layers import MaxPool2D
from keras.layers import Add
from keras.layers import Multiply
from keras.layers import Lambda

from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
from keras.callbacks import ReduceLROnPlateau, EarlyStopping

from keras.regularizers import l2
from keras import backend as K
import keras as keras

from keras.models import load_model

from sklearn.model_selection import KFold
from sklearn.model_selection import StratifiedKFold
import tensorflow as tf

from utils.tf_callbacks import recall_m, precision_m, f1_m, PlotLosses


def make_generators(train_df, val_df, batch_size, target, target_size, directory):
        def train_transform(image):
            image = image.astype(np.float64)
            aug = A.Compose([
                # A.FromFloat(dtype='uint8'),
                # A.CLAHE(always_apply=True),
                # A.Normalize(mean=(0.5076), std=(0.2779)),
                A.HorizontalFlip(p=0.2),
            ])
            return aug(image=image)['image']

        def test_transform(image):
            image = image.astype(np.float64)
            return image

    # define transforms

        train_datagen=ImageDataGenerator(
            preprocessing_function=train_transform,
            rescale=1/255.0)

        test_datagen=ImageDataGenerator(
            preprocessing_function=test_transform,
            rescale=1/255.0)

        train_generator=train_datagen.flow_from_dataframe(dataframe=train_df, directory=directory,
                                                    x_col="filename", y_col=target, class_mode="categorical", target_size=target_size, color_mode='grayscale',
                                                    batch_size=batch_size)


        val_generator=test_datagen.flow_from_dataframe(dataframe=val_df, directory=directory,
                                                    x_col="filename", y_col=target, class_mode="categorical", target_size=target_size, color_mode='grayscale',
                                                    batch_size=batch_size)

        return train_generator, val_generator


def residual_block(input, input_channels=None, output_channels=None, kernel_size=(3, 3), stride=1, regularization=1e-3):
    '''
    Source: https://github.com/qubvel/residual_attention_network
    '''
    if output_channels is None:
        output_channels = input.get_shape()[-1]
    if input_channels is None:
        input_channels = output_channels // 4

    regularizer = l2(regularization)

    strides = (stride, stride)

    x = Activation('relu')(input)
    x = BatchNormalization()(x)
    x = Conv2D(input_channels, (1, 1), kernel_regularizer=regularizer)(x)

    x = Activation('relu')(x)
    x = BatchNormalization()(x)
    x = Conv2D(input_channels, kernel_size, padding='same', kernel_regularizer=regularizer,  strides=stride)(x)

    x = Activation('relu')(x)
    x = BatchNormalization()(x)
    x = Conv2D(output_channels, (1, 1), padding='same', kernel_regularizer=regularizer)(x)

    if input_channels != output_channels or stride != 1:
        input = Conv2D(output_channels, (1, 1), padding='same', kernel_regularizer=regularizer, strides=strides)(input)

    x = Add()([x, input])
    return x


def attention_block(input, input_channels=None, output_channels=None, encoder_depth=1):
    '''
    Source: https://github.com/qubvel/residual_attention_network
    '''

    p = 1
    t = 2
    r = 1

    if input_channels is None:
        input_channels = input.get_shape()[-1]
    if output_channels is None:
        output_channels = input_channels

    # First Residual Block
    for i in range(p):
        input = residual_block(input)

    # Trunc Branch
    output_trunk = input
    for i in range(t):
        output_trunk = residual_block(output_trunk)


    # Soft Mask Branch
    ## encoder
    ### first down sampling
    output_soft_mask = MaxPool2D(padding='same')(input)  # 32x32
    for i in range(r):
        output_soft_mask = residual_block(output_soft_mask)

    skip_connections = []
    for i in range(encoder_depth - 1):

        ## skip connections
        output_skip_connection = residual_block(output_soft_mask)
        skip_connections.append(output_skip_connection)
        # print ('skip shape:', output_skip_connection.get_shape())

        ## down sampling
        output_soft_mask = MaxPool2D(padding='same')(output_soft_mask)
        for _ in range(r):
            output_soft_mask = residual_block(output_soft_mask)


    ## decoder
    skip_connections = list(reversed(skip_connections))
    for i in range(encoder_depth - 1):
        ## upsampling666
        for _ in range(r):
            output_soft_mask = residual_block(output_soft_mask)
        output_soft_mask = UpSampling2D()(output_soft_mask)
        ## skip connections
        output_soft_mask = Add()([output_soft_mask, skip_connections[i]])

    ### last upsampling
    for i in range(r):
        output_soft_mask = residual_block(output_soft_mask)
    output_soft_mask = UpSampling2D()(output_soft_mask)

    ## Output
    output_soft_mask = Conv2D(input_channels, (1, 1))(output_soft_mask)
    output_soft_mask = Conv2D(input_channels, (1, 1))(output_soft_mask)
    output_soft_mask = Activation('sigmoid')(output_soft_mask)

    # Attention: (1 + output_soft_mask) * output_trunk
    output = Lambda(lambda x: x + 1)(output_soft_mask)
    output = Multiply()([output, output_trunk])  #

    # Last Residual Block
    for i in range(p):
        output = residual_block(output)

    return output


def AttentionResNet56(shape, n_channels=64, n_classes=2,
                      dropout=0.2, regularization=1e-3):
    '''
    Source: https://github.com/qubvel/residual_attention_network
    '''
    regularizer = l2(regularization)

    input_ = Input(shape=shape)
    x = Conv2D(n_channels, (7, 7), strides=(2, 2), padding='same')(input_) # 112x112
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = MaxPool2D(pool_size=(3, 3), strides=(2, 2), padding='same')(x)  # 56x56
    
    x = residual_block(x, output_channels=n_channels * 4)  # 56x56
    x = attention_block(x, encoder_depth=4)  # bottleneck 7x7

    x = residual_block(x, output_channels=n_channels * 8)  # 56x56
    x = attention_block(x, encoder_depth=3)  # bottleneck 7x7

    x = residual_block(x, output_channels=n_channels * 16, stride=2)  # 28x28
    x = attention_block(x, encoder_depth=2)  # bottleneck 7x7

    x = residual_block(x, output_channels=n_channels * 32, stride=2)  # 14x14
    x = attention_block(x, encoder_depth=1)  # bottleneck 7x7

    x = residual_block(x, output_channels=n_channels * 64, stride=2)  # 7x7
    x = residual_block(x, output_channels=n_channels * 64)
    x = residual_block(x, output_channels=n_channels * 64)

    pool_size = (x.get_shape()[1], x.get_shape()[2])
    x = AveragePooling2D(pool_size=pool_size, strides=(1, 1))(x)
    x = Flatten()(x)
    if dropout:
        x = Dropout(dropout)(x)
    output = Dense(n_classes, kernel_regularizer=regularizer, activation='softmax')(x)
    
    model = Model(input_, output)
    return model


class PlotLosses(keras.callbacks.Callback):

    def on_train_begin(self, logs={}):
        self.i = 0
        self.j = 0
        self.x = []
        self.y = []
        self.losses = []
        self.val_losses = []
        self.acc = []
        self.val_acc = []
        self.logs = []

    def on_epoch_end(self, epoch, logs={}):
        self.logs.append(logs)
        self.x.append(self.i)
        self.losses.append(logs.get('loss'))
        self.acc.append(logs.get('accuracy'))
        self.val_losses.append(logs.get('val_loss'))
        self.val_acc.append(logs.get('val_accuracy'))
        self.i += 1
        self.j += 1
        self.y.append(self.j)

        plt.plot(self.x, self.losses, label="Training loss")
        plt.plot(self.x, self.val_losses, label="Validation loss")
        plt.legend(['Training Loss','Validation Loss'])
        plt.grid()
        plt.savefig('/home/ubuntu/ran_model_loss_{}_fold-{}.png'.format(train_size, fold_no))
        plt.close()
        plt.plot(self.y, self.acc, label="Training accuracy")
        plt.plot(self.y, self.val_acc, label="Validation accuracy")
        plt.legend(['Training Accuracy','Validation Accuracy'])
        plt.grid()
        plt.savefig('/home/ubuntu/ran_accuracy_{}_fold-{}.png'.format(train_size, fold_no))
        plt.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Residual Attention Net Training Script')

    parser.add_argument('--epochs', default=1000, type=int, help='Number of epochs')
    parser.add_argument('--lr', default=1e-4, type=float, help='Learning rate')
    parser.add_argument('--bs', default=16, type=int, help='Batch size')
    parser.add_argument('--img_size', default=320, type=int, help='Image size')
    parser.add_argument('--img_channel', default=1, type=int, help='Image channel')
    parser.add_argument('--data_csv', default='/MULTIX/DATA/HOME/custom_data_ablation_1500.csv', type=str, help='Path to data file')
    parser.add_argument('--save_dir', default='/home/ubuntu/', type=str, help='Name of folder to store training checkpoints')
    parser.add_argument('--data_dir', default='/home/ubuntu/custom_data/', type=str, help='Path to data folder')
    parser.add_argument('--pneum_weight', default=1., type=float, help='Class weighting for pneumonia')
    parser.add_argument('--neg_weight', default=1., type=float, help='Class weighting for non-pneumonia')
    parser.add_argument('--savefile', default='residual_attn', help='Filename for saved weights')

    args = parser.parse_args()

    total_data = pd.read_csv(args.data_csv, dtype=str)

    train_size = len(total_data[total_data['split']=='train'])

    for col, row in total_data.iterrows():
      if row['split']=='val':
        row['split']='train'

    train_df = total_data[total_data['split']=='train']
    train_df = train_df.reset_index(drop=True)

    test_df = total_data[total_data['split']=='test']
    test_df = test_df.reset_index(drop=True)

    # build a model
    model = AttentionResNet56(shape=(args.img_size, args.img_size, args.img_channel), n_channels=args.img_channel)

    def get_model_name(k):
        return args.savefile + str(k)+'.h5'

    seed = 0
    np.random.seed(seed)
    kfold = StratifiedKFold(n_splits=3, shuffle=True, random_state=seed)
    target = train_df.pneumonia_binary

    class_weights = {0:args.neg_weight, 1:args.pneum_weight}
    print('class weights: ', class_weights)

    cv_accuracy = []
    cv_loss = []

    fold_no = 1
    for train_idx, val_idx in kfold.split(train_df, target):
        # print('TRAIN: ', train_idx, 'TEST: ', val_idx)
        train_kfold = train_df.iloc[train_idx]
        val_kfold = train_df.iloc[val_idx]

        train_generator, val_generator = make_generators(train_kfold, val_kfold, batch_size=args.bs, 
            target='pneumonia_binary',target_size=(args.img_size,args.img_size), directory=args.data_dir)
        STEP_SIZE_TRAIN=train_generator.n//train_generator.batch_size
        STEP_SIZE_VALID=val_generator.n//val_generator.batch_size

            # prepare usefull callbacks
        filepath = args.save_dir+get_model_name(fold_no)
        checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='min')

        early_stopper = EarlyStopping(monitor='val_loss', min_delta=0, patience=50, verbose=1)
        lr_reducer = ReduceLROnPlateau(monitor='val_loss', factor=0.9, patience=5, min_lr=10e-9, epsilon=0.01, verbose=1)
        plot_losses = PlotLosses()

        callback = [checkpoint, early_stopper, lr_reducer, plot_losses]

        with tf.device('/device:GPU:0'):
            model.compile(keras.optimizers.Adam(lr=args.lr), loss='categorical_crossentropy', metrics=['accuracy', f1_m, precision_m, recall_m])
            print('------------------------------------------------------------------------')
            print(f'Training for fold {fold_no} ...')
            history = model.fit(train_generator, steps_per_epoch=STEP_SIZE_TRAIN, epochs=args.epochs, validation_data=val_generator, 
                validation_steps=STEP_SIZE_VALID, callbacks=callback, class_weight=class_weights)
            model.load_weights(filepath)
            scores = model.evaluate(val_generator, steps=STEP_SIZE_VALID)
            print(f'Score for fold {fold_no}: {model.metrics_names[0]} of {scores[0]}; {model.metrics_names[1]} of {scores[1]*100}%')

            cv_accuracy.append(scores[1]*100)
            cv_loss.append(scores[0])

            # keras.backend.clear_session()

            fold_no = fold_no + 1


    # == Provide average scores ==
    print('------------------------------------------------------------------------')
    print('Score per fold')
    for i in range(0, len(cv_accuracy)):
      print('------------------------------------------------------------------------')
      print(f'> Fold {i+1} - Loss: {cv_loss[i]} - Accuracy: {cv_accuracy[i]}%')
    print('------------------------------------------------------------------------')
    print('Average scores for all folds:')
    print(f'> Accuracy: {np.mean(cv_accuracy)} (+- {np.std(cv_accuracy)})')
    print(f'> Loss: {np.mean(cv_loss)}')
    print('------------------------------------------------------------------------')



